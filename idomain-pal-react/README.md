This project is expected to run on local host and connected to the Google Firebase Firestore. Please set up your React developing/running environment and create a Firebase project for your server side.

## Install Homebrew (if applicable)

Homebrew is “The missing package manager for macOS”.

### $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

### $ brew -v

## Install Node via Homebrew

### $ brew install node

### $ node -v

### $ npm -v

## Create your Firebase project (if applicable)

Go to your console of firebase, and create a project called iDomainPal. Open this project and enable Email/Password, Google, and Facebook sign-in methods.<br><br>

Create a Firestore Database.<br><br>

Copy the config info from your project settings -> web app to the file .env in the root directory.

## Run the project

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### $ cd idomain-pal-react

### $ npm start

