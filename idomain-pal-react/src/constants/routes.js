export const SIGN_UP = "/register";
export const SIGN_IN = "/login";
export const LANDING = "/";
export const DASHBOARD = "/dashboard";

export const DOMAIN = "/domain";
export const DOMAIN_NEW = "/domain/new";
export const DOMAIN_SHOW = "/domain/show/:id";
export const DOMAIN_EDIT = "/domain/edit/:id";

export const PROVIDER = "/provider";
export const PROVIDER_NEW = "/provider/new";
export const PROVIDER_DETAIL = "/provider/show/:id";
export const PROVIDER_EDIT = "/provider/edit/:id";

export const ACCOUNT = "/account";
// export const ADMIN = "/admin";
// export const ADMIN_DETAILS = "/admin/:id";
export const PASSWORD_FORGOT = "/password-forgot";
