import React from "react";
import { compose } from "recompose";

import {
  AuthUserContext,
  withAuthorization,
  withEmailVerification
} from "../Session";

import PasswordChangeForm from "../PasswordChange";
import LoginManagement from "./LoginManagement";

const AccountPage = () => (
  <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <AuthUserContext.Consumer>
        {authUser => <h1 className="h2">Account: {authUser.email}</h1>}
      </AuthUserContext.Consumer>
      <div className="btn-toolbar mb-2 mb-md-0">
        <button type="button" className="btn btn-sm btn-outline-primary">
          Change Password
        </button>

        <button type="button" className="btn btn-sm btn-outline-primary">
          <span data-feather="calendar" />
          Sign In Methods
        </button>
      </div>
    </div>

    <AuthUserContext.Consumer>
      {authUser => (
        <div className="row">
          <div className="col-md-6 ml-sm-auto">
            <h3>Change Password</h3>
            <PasswordChangeForm />
          </div>
          <div className="col-md-6 ml-sm-auto">
            <h3>Sign In Methods</h3>
            <LoginManagement authUser={authUser} />
          </div>
        </div>
      )}
    </AuthUserContext.Consumer>
  </main>
);

const condition = authUser => !!authUser;

export default compose(
  withEmailVerification,
  withAuthorization(condition)
)(AccountPage);
