import React from "react";

import DomainItem from "./DomainItem";

const DomainList = ({ domains, onEditDomain, onRemoveDomain }) => (
  <ul className="list-group top-to-header">
    {domains.map(domain => (
      <DomainItem
        key={domain.uid}
        domain={domain}
        onEditDomain={onEditDomain}
        onRemoveDomain={onRemoveDomain}
      />
    ))}
  </ul>
);

export default DomainList;
