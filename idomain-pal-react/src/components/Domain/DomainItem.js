import React, { Component } from "react";

class DomainItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      editText: this.props.domain.name
    };
  }

  onToggleEditMode = () => {
    this.setState(state => ({
      editMode: !state.editMode,
      editText: this.props.domain.name
    }));
  };

  onChangeEditText = event => {
    this.setState({ editText: event.target.value });
  };

  onSaveEditText = () => {
    this.props.onEditDomain(this.props.domain, this.state.editText);

    this.setState({ editMode: false });
  };

  render() {
    const { domain, onRemoveDomain } = this.props;
    const { editMode, editText } = this.state;

    return (
      <li className="list-group-item d-flex justify-content-between align-items-center">
        {editMode ? (
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <i className="fas fa-pen-square" />
              </span>
            </div>
            <input
              type="text"
              className="form-control text-md"
              placeholder="Enter your Todo item"
              value={editText}
              onChange={this.onChangeEditText}
            />
            <div className="input-group-append">
              <button
                type="button"
                className="btn-sm btn-outline-primary ml-1"
                onClick={this.onSaveEditText}
              >
                <i className="fas fa-save" />{" "}
              </button>
              <button
                type="button"
                className="btn-sm btn-outline-primary ml-1"
                onClick={this.onToggleEditMode}
              >
                <i className="fas fa-undo-alt" />{" "}
              </button>
            </div>
          </div>
        ) : (
          <div>{domain.name}</div>
        )}

        {!editMode && (
          <div>
            <button
              type="button"
              className="btn-sm btn-outline-primary ml-1 my-sm-0"
              onClick={this.onToggleEditMode}
            >
              <i className="fas fa-pen-square" />{" "}
            </button>
            <button
              type="button"
              className="btn-sm btn-outline-danger ml-1 my-sm-0"
              onClick={() => onRemoveDomain(domain.uid)}
            >
              <i className="fas fa-minus-square" />{" "}
            </button>
          </div>
        )}
      </li>
    );
  }
}

export default DomainItem;
