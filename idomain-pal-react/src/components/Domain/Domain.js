import React, { Component } from "react";

import { AuthUserContext } from "../Session";
import { withFirebase } from "../Firebase";
import DomainList from "./DomainList";

class Domain extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      // registered: "",
      // expired: "",
      // providerName: "",
      // serverIP: "",
      // primaryDNS: "",
      // secondaryDNS: "",
      loading: false,
      domains: []
    };
  }

  componentDidMount() {
    this.onListenToDomains();
  }

  onListenToDomains() {
    this.setState({ loading: true });

    this.unsubscribe = this.props.firebase
      .domains()
      .orderBy("name")
      .onSnapshot(snapshot => {
        if (snapshot.size) {
          let domains = [];
          snapshot.forEach(doc => domains.push({ ...doc.data(), uid: doc.id }));

          this.setState({
            domains: domains.reverse(),
            loading: false
          });
        } else {
          this.setState({ domains: null, loading: false });
        }
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onChangeName = event => {
    this.setState({ name: event.target.value });
  };

  onCreateDomain = (event, authUser) => {
    this.props.firebase.domains().add({
      name: this.state.name,
      // registered: this.state.registered,
      // expired: this.state.expired,
      // providerName: this.state.providerName,
      // serverIP: this.state.serverIP,
      // primaryDNS: this.state.primaryDNS,
      // secondaryDNS: this.state.secondaryDNS,
      userId: authUser.uid
    });

    this.setState({
      name: ""
      // registered: "",
      // expired: "",
      // providerName: "",
      // serverIP: "",
      // primaryDNS: "",
      // secondaryDNS: ""
    });

    event.preventDefault();
  };

  onEditDomain = (domain, name) => {
    this.props.firebase.domain(domain.uid).set({
      ...domain,
      name
      // registered,
      // expired,
      // providerName,
      // serverIP,
      // primaryDNS,
      // secondaryDNS
    });
  };

  onRemoveDomain = uid => {
    console.log("uuuuuuuuuuuuuuu:" + uid);
    this.props.firebase.domain(uid).remove();
  };

  onNextPage = () => {
    this.setState(
      state => ({ limit: state.limit + 5 }),
      this.onListenToDomains
    );
  };

  render() {
    const { name, domains, loading } = this.state;

    return (
      <AuthUserContext.Consumer>
        {authUser => (
          <div>
            {loading && <div>loading...</div>}
            <label> {authUser.email} </label>

            {domains ? (
              <DomainList
                domains={domains.filter(
                  domain => authUser.uid === domain.userId
                )}
                onEditDomain={this.onEditDomain}
                onRemoveDomain={this.onRemoveDomain}
              />
            ) : (
              <div className="alert alert-primary" role="alert">
                There are no domains ...
              </div>
            )}
            <div className="top-to-item">
              <form onSubmit={event => this.onCreateDomain(event, authUser)}>
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-list-alt" />
                    </span>
                  </div>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter your domain"
                    value={name}
                    onChange={this.onChangeName}
                  />
                  <div className="input-group-append">
                    <button
                      type="submit"
                      className="btn btn-outline-primary my-2 my-sm-0"
                    >
                      <i className="fas fa-share-square" /> Add
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        )}
      </AuthUserContext.Consumer>
    );
  }
}

export default withFirebase(Domain);
