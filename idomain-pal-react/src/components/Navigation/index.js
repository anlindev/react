import React from "react";
import { Link, NavLink } from "react-router-dom";

import { AuthUserContext } from "../Session";
import SignOutButton from "../SignOut";
import * as ROUTES from "../../constants/routes";

import "../../assets/styles/dashboard.css";

const Navigation = () => (
  <div>
    <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <Link className="navbar-brand col-sm-3 col-md-2 mr-0" to={ROUTES.LANDING}>
        iDomain Pal
      </Link>
      <input
        className="form-control form-control-dark w-100"
        type="text"
        placeholder="Search"
        aria-label="Search"
      />
      <ul className="navbar-nav px-3">
        <AuthUserContext.Consumer>
          {authUser =>
            authUser ? <NavbarAuth authUser={authUser} /> : <NavbarNonAuth />
          }
        </AuthUserContext.Consumer>
      </ul>
    </nav>

    <div className="container-fluid">
      <div className="row">
        <AuthUserContext.Consumer>
          {authUser => (authUser ? <SidebarAuth authUser={authUser} /> : null)}
        </AuthUserContext.Consumer>
      </div>
    </div>
  </div>
);

const SidebarAuth = ({ authUser }) => (
  <nav className="col-md-2 d-none d-md-block bg-light sidebar">
    <div className="sidebar-sticky">
      <ul className="nav flex-column">
        <li className="nav-item">
          <NavLink
            className="nav-link"
            activeClassName="active"
            to={ROUTES.DASHBOARD}
          >
            <span data-feather="home" /> Dashboard
            <span className="sr-only">(current)</span>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            className="nav-link"
            activeClassName="active"
            to={ROUTES.DOMAIN}
          >
            <span data-feather="file" /> Domain
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            className="nav-link"
            activeClassName="active"
            to={ROUTES.PROVIDER}
          >
            <span data-feather="file" /> Provider
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            className="nav-link"
            activeClassName="active"
            to={ROUTES.ACCOUNT}
          >
            <span data-feather="bar-chart-2" /> Account
          </NavLink>
        </li>
      </ul>
    </div>
  </nav>
);

const NavbarAuth = () => (
  <li className="nav-item text-nowrap">
    <SignOutButton />
  </li>
);

const NavbarNonAuth = () => (
  <li className="nav-item text-nowrap">
    <Link
      className="nav-item btn btn-md-custom btn-outline-success my-2 my-sm-0"
      to={ROUTES.SIGN_IN}
    >
      Sign In
    </Link>
  </li>
);

export default Navigation;
