import React from "react";

import ProviderItem from "./ProviderItem";

const ProviderList = ({ providers, onEditProvider, onRemoveProvider }) => (
  <ul className="list-group top-to-header">
    {providers.map(provider => (
      <ProviderItem
        key={provider.uid}
        provider={provider}
        onEditProvider={onEditProvider}
        onRemoveProvider={onRemoveProvider}
      />
    ))}
  </ul>
);

export default ProviderList;
