import React, { Component } from "react";

import { AuthUserContext } from "../Session";
import { withFirebase } from "../Firebase";
import ProviderList from "./ProviderList";

class Provider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      // url: "",
      // username: "",
      // password: "",
      // email: "",
      loading: false,
      providers: []
    };
  }

  componentDidMount() {
    this.onListenToProviders();
  }

  onListenToProviders() {
    this.setState({ loading: true });

    this.unsubscribe = this.props.firebase
      .providers()
      .orderBy("name")
      .onSnapshot(snapshot => {
        if (snapshot.size) {
          let providers = [];
          snapshot.forEach(doc =>
            providers.push({ ...doc.data(), uid: doc.id })
          );

          this.setState({
            providers: providers.reverse(),
            loading: false
          });
        } else {
          this.setState({ providers: null, loading: false });
        }
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onChangeName = event => {
    this.setState({ name: event.target.value });
  };

  onCreateProvider = (event, authUser) => {
    this.props.firebase.providers().add({
      name: this.state.name,
      // url: this.state.url,
      // username: this.state.username,
      // password: this.state.password,
      // email: this.state.email,
      userId: authUser.uid
    });

    this.setState({
      name: ""
      // url: "",
      // username: "",
      // password: "",
      // email: ""
    });

    event.preventDefault();
  };

  onEditProvider = (provider, name) => {
    this.props.firebase.provider(provider.uid).set({
      ...provider,
      name
      // url,
      // username,
      // password,
      // email
    });
  };

  onRemoveProvider = uid => {
    this.props.firebase.provider(uid).remove();
  };

  onNextPage = () => {
    this.setState(
      state => ({ limit: state.limit + 5 }),
      this.onListenToProviders
    );
  };

  render() {
    const { name, providers, loading } = this.state;

    return (
      <AuthUserContext.Consumer>
        {authUser => (
          <div>
            {loading && <div>loading...</div>}
            <label> {authUser.email} </label>

            {providers ? (
              <ProviderList
                providers={providers.filter(
                  provider => authUser.uid === provider.userId
                )}
                onEditProvider={this.onEditProvider}
                onRemoveProvider={this.onRemoveProvider}
              />
            ) : (
              <div className="alert alert-primary" role="alert">
                There are no providers ...
              </div>
            )}

            <div className="top-to-item">
              <form onSubmit={event => this.onCreateProvider(event, authUser)}>
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-list-alt" />
                    </span>
                  </div>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter your provider"
                    value={name}
                    onChange={this.onChangeName}
                  />
                  <div className="input-group-append">
                    <button
                      type="submit"
                      className="btn btn-outline-primary my-2 my-sm-0"
                    >
                      <i className="fas fa-share-square" /> Add
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        )}
      </AuthUserContext.Consumer>
    );
  }
}

export default withFirebase(Provider);
